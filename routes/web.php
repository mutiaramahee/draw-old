<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('draw_layout.index');
});

Route::get('admin/event/preferences/{id}', 'AdminEventController@preferences')->name('preferences');
Route::post('admin/event/save-preferences/{id}', 'AdminEventController@savePreferences')->name('save-preferences');

Route::get('admin/category_disabled/add_selected_participant', 'AdminCategoryDisabledController@addSelectedParticipant')->name('add-selected-participant');
Route::get('admin/category_disabled/add_selected_participant/{id}', 'AdminCategoryDisabledController@getCategory');
Route::get('admin/category_disabled/participant/{id}', 'AdminCategoryDisabledController@getParticipant');
Route::post('admin/category_disabled/save_disabled_category', 'AdminEventController@saveDisabledCategory');