@extends('draw_template.content')

@section('content')

	<!-- Main Content -->
	<div class="main-content">
		<!-- Main -->
		<div class="main text-center font-weight-bold">
			<div class="main-head">
				<h1 class="mb-0">Undian 24 Tiket Gratis ke Seoul Ketemu BTS</h1>
				<hr class="line-title col-md-8 mt-0"></hr>
			</div>

			<div class="main-body text-center">
				<!-- Draw Button -->
				<a href="#"><img src="assets/image/img_draw_button.png" class="draw"></a>
			</div>

			<div class="main-footer">
				<h4 class="text-uppercase">Draw and find the winner!</h4>
			</div>
		</div>
		<!-- End of Main -->
	</div>
	<!-- End of Main Content -->

@endsection