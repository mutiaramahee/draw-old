<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->

<style type="text/css">
  .required{
    color: red;
    font-weight: bold;
  }
</style>

<div class='panel panel-default'>
    <div class="panel-body">
        <form method="POST" action="{{CRUDBooster::mainpath('save_disabled_category')}}">
          @csrf
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                Event<span class="required">*</span>
                <select name="event_id" class="form-control">
                  <option>** Please select event</option>
                  @foreach($event as $row)
                    <option value="{{$row->id}}">{{$row->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                Category<span class="required">*</span>
                <select name="category_id" class="form-control">
                    <option>** Please select category</option>
                </select>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group"><br>
                <input type="submit" name="submit" value="Add Data" class="btn btn-success">
              </div>
            </div>

          </div>
          <hr>
          @include('superadmin.participant_data')
          <!-- <table class='table table-striped table-bordered' id="dataParticipant">
            <thead>
                <tr>
                  <th style="width: 30px"></th>
                  <th>Event Name</th>
                  <th>Name</th>
                  <th>Email</th>
                 </tr>
            </thead>
            <tbody>
                <tr>
                  <td colspan="4" class="text-center">No Data Available</td>
                </tr>
            </tbody>
          </table> -->

          <!-- ADD A PAGINATION -->
          <p>{!! urldecode(str_replace("/?","?",$participant->appends(Request::all())->render())) !!}</p>
      </form>
    </div>
</div>
@endsection

@prepend('bottom')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://unpkg.com/axios@0.20.0-0/dist/axios.min.js"></script>

<script type="text/javascript">

    // $(document).ready(function(){
    //   $('select[name="event_id"]').on('change', function() {
    //           var event = $(this).val();

    //           // select
    //           $('select[name="category_id"]').empty();
    //           $('select[name="category_id"]').append('<option>** Please wait...</option>');
    //           if(event) {
    //               $.ajax({
    //                   url: "{{ url('/admin/category_disabled/add_selected_participant') }}"+'/'+event,
    //                   type: "GET",
    //                   dataType: "json",
    //                   success:function(data) {
    //                       $('select[name="category_id"]').empty();
    //                       if(data.length == 0){
    //                         $('select[name="category_id"]').append('<option>** No category available!</option>');
    //                       }
    //                       $.each(data, function(i, item) {
    //                           $('select[name="category_id"]').append('<option value="'+ data[i].id +'">'+ data[i].name +'</option>');
    //                       });
    //                   },
    //               });

    //               // table
    //               $('#dataParticipant > tbody').empty();
    //               $('#dataParticipant > tbody:last-child').append('<tr><td colspan="4" class="text-center">Load the data... </td></tr>');
    //               $.ajax({
    //                   url: "{{ url('/admin/category_disabled/participant') }}"+'/'+event,
    //                   type: "GET",
    //                   dataType: "json",
    //                   success:function(data) {
    //                       $('#dataParticipant > tbody').empty();
    //                       if(data.length == 0){
    //                         $('#dataParticipant > tbody:last-child').append('<tr><td colspan="4" class="text-center">No data available</td></tr>');
    //                       }
    //                       $.each(data, function(i, item) {
    //                           $('#dataParticipant > tbody:last-child').append('<tr><td><input type="checkbox" name="selected_id[]" value="'+data[i].id+'"></td><td>'+data[i].event_name+'</td><td>'+data[i].name+'</td><td>'+data[i].email+'</td></tr>');
    //                       });
    //                   },
    //               });
    //           } else {
    //               // select
    //               $('select[name="category_id"]').empty();
    //               $('select[name="category_id"]').append('<option>** Please select category</option>');

    //               // table
    //               $('#dataParticipant > tbody').empty();
    //               $('#dataParticipant > tbody:last-child').append('<tr><td colspan="4" class="text-center">No data available</td></tr>');
    //           }
    //       });
    // });
</script>
@endprepend
