<template>
<div id="dataParticipant">	
	<table class='table table-bordered table-striped'>
        <thead>
            <tr>
                <th style="width: 30px"></th>
                <th>Event Name</th>
                <th>Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="row in participants">
            	<td><input type="checkbox" name="selected_id[]" value="@{{row.id}}"></td>
            	<td>@{{row.event_name}}</td>
            	<td>@{{row.name}}</td>
            	<td>@{{row.email}}</td>
   			</tr>
        </tbody>
    </table>
</div>
</template>

@push('bottom')
<script type="text/javascript">
	new Vue({
		el: '#dataParticipant',
		data: {
			participants: [],
			row: {
				id: '',
				created_at: '',
				updated_at: '',
				name: '',
				email: '',
				phone: '',
				event_id: '',
				event_name: ''
			}
		},
		created() { 
			this.fetchData();
		},
		methods: {
			fetchData() {
				console.log('tes');
				// fetch("participant/1")
				// 	.then(response => response.json())
				// 	.then(response => {
				// 		this.participants = response;
				// 	})
				// 	.catch(err => console.log(err));

				// console.log('data' + this.participants);
				// axios.get("participant/1")
				// 	.then(response => (this.participants = response));
			}
		}
	});
</script>
@endpush('bottom')